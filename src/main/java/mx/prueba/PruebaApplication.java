package mx.prueba;

import jakarta.annotation.PostConstruct;
import mx.prueba.dataaccess.CatalogoPrecioRepository;
import mx.prueba.dataaccess.documents.CatalogoPrecio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.List;

@SpringBootApplication
public class PruebaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebaApplication.class, args);
    }

    @Autowired
    CatalogoPrecioRepository catalogoPrecioRepository;

    @PostConstruct
    public void init() {

        List<CatalogoPrecio> catalogo = List.of(new CatalogoPrecio("001", "Oro puro 24k", BigDecimal.valueOf(1500))
                , new CatalogoPrecio("002", "Oro alto 18k", BigDecimal.valueOf(1000))
                , new CatalogoPrecio("003", "Oro medio 14k", BigDecimal.valueOf(800))
                , new CatalogoPrecio("004", "Oro bajo 10k", BigDecimal.valueOf(500))
                , new CatalogoPrecio("005", "Plata ley .925", BigDecimal.valueOf(300))
                , new CatalogoPrecio("006", "Titanio", BigDecimal.valueOf(200))
                , new CatalogoPrecio("007", "Rodio", BigDecimal.valueOf(100)));

        catalogoPrecioRepository.saveAll(catalogo);
    }
}
