package mx.prueba.dataaccess.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(collection = "catalogoPrecioCollection")
public class CatalogoPrecio {
    @Id
    private String id;
    private String material;
    private BigDecimal precioGramo;

    public CatalogoPrecio() {
    }

    public CatalogoPrecio(String id, String material, BigDecimal precioGramo) {
        this.id = id;
        this.material = material;
        this.precioGramo = precioGramo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public BigDecimal getPrecioGramo() {
        return precioGramo;
    }

    public void setPrecio(BigDecimal precioGramo) {
        this.precioGramo = precioGramo;
    }

    @Override
    public String toString() {
        return "CatalogoPrecio{" +
                "id='" + id + '\'' +
                ", material='" + material + '\'' +
                ", precioGramo=" + precioGramo +
                '}';
    }
}
