package mx.prueba.dataaccess;

import mx.prueba.dataaccess.documents.CatalogoPrecio;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogoPrecioRepository extends MongoRepository<CatalogoPrecio, String> {
}
