package mx.prueba.controller;

import mx.prueba.exception.PruebaTecnicaException;
import mx.prueba.service.Calculadora;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class PrestamoEvaluoController {

    @Autowired
    private Calculadora calculadora;

    @GetMapping("/prestamo")
    public ResponseEntity<Object> prestamoImporte(@RequestParam String idMaterial, @RequestParam Integer peso) {
        try {
            return new ResponseEntity<>(Pair.of("importe", calculadora.montoPrestamo(idMaterial, peso)), HttpStatus.OK);
        } catch (PruebaTecnicaException e) {
            return new ResponseEntity<>(Pair.of("mensaje", e.getMessage()), HttpStatus.NOT_FOUND);
        }
    }
}
