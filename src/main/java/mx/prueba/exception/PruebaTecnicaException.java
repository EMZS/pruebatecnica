package mx.prueba.exception;

public class PruebaTecnicaException extends Exception{

    public PruebaTecnicaException(final String message) {
        super(message);
    }

}
