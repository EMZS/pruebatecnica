package mx.prueba.service;

import mx.prueba.dataaccess.CatalogoPrecioRepository;
import mx.prueba.dataaccess.documents.CatalogoPrecio;
import mx.prueba.exception.PruebaTecnicaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class Calculadora {

    private CatalogoPrecioRepository catalogoPrecioRepository;
    private static final BigDecimal PORCENTAJE = BigDecimal.valueOf(0.8);

    @Autowired
    public Calculadora(CatalogoPrecioRepository catalogoPrecioRepository) {
        this.catalogoPrecioRepository = catalogoPrecioRepository;
    }

    public BigDecimal montoPrestamo(String id, Integer gramos) throws PruebaTecnicaException {
        Optional<CatalogoPrecio> findById = catalogoPrecioRepository.findById(id);
        BigDecimal result = BigDecimal.ZERO;

        if (!findById.isPresent()) {
            throw new PruebaTecnicaException("Id de material no encontrado");
        }

        CatalogoPrecio catalogoPrecio = findById.get();
        result = result.add(BigDecimal.valueOf(gramos).multiply(catalogoPrecio.getPrecioGramo()).multiply(PORCENTAJE));
        return result;
    }
}
