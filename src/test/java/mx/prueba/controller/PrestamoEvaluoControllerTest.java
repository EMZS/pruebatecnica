package mx.prueba.controller;

import mx.prueba.exception.PruebaTecnicaException;
import mx.prueba.service.Calculadora;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class PrestamoEvaluoControllerTest {

    private static final String ID = "001";
    private static final Integer PESO = 123;

    @Mock
    private Calculadora calculadora;

    @InjectMocks
    private PrestamoEvaluoController prestamoEvaluoController;

    @Test
    void should_return_error_given_params_when_get_importe_throw_error() throws PruebaTecnicaException {
        //GIVEN
        assertThat(prestamoEvaluoController).isNotNull();
        given(calculadora.montoPrestamo(anyString(), anyInt()))
                .willThrow(new RuntimeException("Unit Test"));
        //WHEN
        final Throwable response = catchThrowable(() -> prestamoEvaluoController.prestamoImporte(ID, PESO));

        //THEN
        assertThat(response).isNotNull().isInstanceOf(RuntimeException.class);
    }

    @Test
    void should_return_data_given_params_when_get_importe_obtain_result() throws PruebaTecnicaException {
        //GIVEN
        assertThat(prestamoEvaluoController).isNotNull();
        given(calculadora.montoPrestamo(anyString(), anyInt()))
                .willReturn(BigDecimal.TEN);
        //WHEN
        ResponseEntity<Object> response = prestamoEvaluoController.prestamoImporte(ID, PESO);

        //THEN
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        assertThat(response.getBody()).isNotNull();
    }
}