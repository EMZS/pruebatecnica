package mx.prueba.service;

import mx.prueba.dataaccess.CatalogoPrecioRepository;
import mx.prueba.dataaccess.documents.CatalogoPrecio;
import mx.prueba.exception.PruebaTecnicaException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class CalculadoraTest {

    private static final String ID = "001";
    private static final Integer PESO = 123 ;

    @Mock
    private CatalogoPrecioRepository catalogoPrecioRepository;

    @InjectMocks
    private Calculadora calculadora;

    @Test
    void should_return_error_given_params_when_catalogoPrecioRepository_throw_error() {
        //GIVEN
        assertThat(calculadora).isNotNull();
        given(catalogoPrecioRepository.findById(anyString()))
                .willThrow(new RuntimeException("Unit Test"));

        //WHEN
        final Throwable result = catchThrowable(()->  calculadora.montoPrestamo(ID,PESO));

        //THEN
        assertThat(result).isNotNull().isInstanceOf(RuntimeException.class);
    }

    @Test
    void should_return_error_given_params_when_catalogoPrecioRepository_not_found_data() {
        //GIVEN
        assertThat(calculadora).isNotNull();
        given(catalogoPrecioRepository.findById(anyString()))
                .willReturn(Optional.empty());

        //WHEN
        final Throwable result = catchThrowable(()->  calculadora.montoPrestamo(ID,PESO));

        //THEN
        assertThat(result).isNotNull().isInstanceOf(PruebaTecnicaException.class);
        assertThat(result.getMessage()).isEqualTo("Id de material no encontrado");
    }

    @Test
    void should_return_value_given_params_when_catalogoPrecioRepository_found_data() throws PruebaTecnicaException {
        //GIVEN
        assertThat(calculadora).isNotNull();
        given(catalogoPrecioRepository.findById(anyString()))
                .willReturn(Optional.of(mockCatalogoPrecio()));

        //WHEN
        final BigDecimal result =  calculadora.montoPrestamo(ID,PESO);

        //THEN
        assertThat(result).isNotNull().isInstanceOf(BigDecimal.class);
    }

    private CatalogoPrecio mockCatalogoPrecio() {
        CatalogoPrecio cat= new CatalogoPrecio();
        cat.setId(ID);
        cat.setMaterial("Oro 24k");
        cat.setPrecio(BigDecimal.TEN);
                return cat;
    }

}