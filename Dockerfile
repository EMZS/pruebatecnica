# Stage 1: build application
FROM maven:3.8.4-openjdk-17 AS build
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean install

# Sateg 2: Run application
FROM openjdk:17-alpine
WORKDIR /APP
COPY --from=build /app/target/PruebaTecnica-1.0-SNAPSHOT.jar ./PruebaTecnica-aws.jar
EXPOSE 8080
CMD ["java","-jar","PruebaTecnica-aws.jar"]
