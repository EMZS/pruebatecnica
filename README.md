# EXAMEN EMPEÑOS

## Tareas completadas.
###Nivel 1.
- Realiza un programa en cualquiera de los lenguajes de programación mencionados que cumpla con las especificaciones solicitadas por el valuador
- Publica el servicio en una nube cloud computing libre (Google GCP, Microsoft Azure o Amazon AWS) para invocar el servicio mencionado
* http://3.16.207.236/PruebaTecnica
  * recurso 
    * /prestamo
  * parametros 
    * idMaterial (string)
    * peso (integer) 
- Comparte la URL de tu repositorio de código en Github y un ejemplo para ejecutarlo
  url: https://gitlab.com/EMZS/pruebatecnica
  
###Nivel 2.
-Integra el proceso de despliegue continuo con un pipeline CI-CD, puedes usar Jenkins, Azure Pipelines o GitHub Actions 
-Guarda los parámetros que consumes de la tabla de precios en una base de datos NO_SQL como MongoDB o Elastic Search
  * https://drive.google.com/file/d/1mb32PnmePPR88lPpQjBC92lav1URXLGS/view?usp=drive_link
Comparte tu documentación en algún programa de API Manager Documentation como Apiary, Swagger, MuleSoft
http://3.16.207.236/PruebaTecnica/swagger-ui.html
Comparte un script de ejecución en SoapUI, Postman, Swagger o similar
  * Curl: 
    * curl --location 'http://3.16.207.236/PruebaTecnica/prestamo?idMaterial=002&peso=4'

###Nivel 3.
Despliega un front de consulta, web app o app móvil con una UI-UX responsiva para invocar la API de préstamos
* http://3.16.207.236/PruebaTecnica

Integra tu código a SonarQube para conocer la calidad de lo realizado

*https://drive.google.com/file/d/1B3FrLTsM9pxCkg6SdgTrTMb7aaFTRD9W/view?usp=drive_link
